package httpexample.potingchiang.com.servertalk_post.Object;

/**
 * This class is for person info object
 * Created by potingchiang on 2016-06-28.
 */
public class Person {

    private String name;
    private String country;
    private String twitter;

    //getter & setter
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getTwitter() {
        return twitter;
    }
    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }
}
