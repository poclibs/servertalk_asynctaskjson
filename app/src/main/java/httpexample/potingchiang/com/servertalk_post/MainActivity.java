package httpexample.potingchiang.com.servertalk_post;

import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import httpexample.potingchiang.com.mylibrary.BasicHttpConnection_Json;
import httpexample.potingchiang.com.servertalk_post.Object.Person;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    //declaration
    //layout elements
    private TextView tvIsConnected;
    private EditText etName, etCountry, etTwitter;
    private Button btnPost;
    //object
    private Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init layout elements
        tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
        etName = (EditText) findViewById(R.id.etName);
        etCountry = (EditText) findViewById(R.id.etCountry);
        etTwitter = (EditText) findViewById(R.id.etTwitter);
        btnPost = (Button) findViewById(R.id.btnPost);
        //setup on click listener
        btnPost.setOnClickListener(this);
        //setup message for connection
        if (isConnected()) {

            //bc color
            tvIsConnected.setBackgroundColor(0xFF00CC00);
            //setup text
            tvIsConnected.setText("Connected");
        }
        else {

            tvIsConnected.setText("Not connected");
        }

    }

    @Override
    public void onClick(View v) {

        //init. person object
        person = new Person();
        person.setName(etName.getText().toString());
        person.setCountry(etCountry.getText().toString());
        person.setTwitter(etTwitter.getText().toString());
        //execute async task
        new HttpAsyncTask().execute("http://hmkcode.appspot.com/jsonservlet");
    }

    //other methods
    public boolean isConnected() {

        //init is connected boolean
        boolean b_IsConnected = false;
        //init. connection manager
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        //get active network info
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        //check if its connected
        if (networkInfo != null && networkInfo.isConnected()) {

            b_IsConnected = true;
        }

        return b_IsConnected;
    }

    //request/post to server
//    public static String POST(String url, Person person) {
    public String POST(String url, Person person) {

        //init. result
        String result = "";
        //init. Output/Input stream
        DataOutputStream dataOutputStream;
//        InputStream inputStream;
        //lib test
        BasicHttpConnection_Json myConn = null;

        try {


            //init connection
            myConn = new BasicHttpConnection_Json(MainActivity.this, url, "POST");

//            //init url object
//            URL mUrl = new URL(url);
//            //init. connection
//            HttpURLConnection conn = (HttpURLConnection) mUrl.openConnection();
//            //setup connection method & properties
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setUseCaches(false);
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Connection", "keep-alive");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestProperty("Content-Type", "application/json");

            //init. output data/json string
            String jSon;
            //init. post data/person object
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("name", person.getName());
            jsonObject.accumulate("country", person.getCountry());
            jsonObject.accumulate("twitter", person.getTwitter());
            //setup json string
            jSon = jsonObject.toString();

            //init data output stream
            dataOutputStream = myConn.getDataOutputStream();
            //write data into output stream
            //??
            dataOutputStream.writeBytes(jSon);

//            //init. input stream
//            inputStream = conn.getInputStream();

            //init. input stream within buffer reader
            BufferedReader bufferedReader = myConn.getBufferedReader();
            if (bufferedReader != null) {

                result = convertInputStreamToString(bufferedReader);
            }
            else {

                result = "Not working!!!";
            }

        } catch (Exception e) {

            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        //close stream
        if (myConn != null) {

            myConn.close();
        }

        //return result
        return result;
    }
    //response from server
//    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
    private String convertInputStreamToString(BufferedReader bufferedReader) throws IOException {

        //result
        String result = "";
        //int. buffer reader to get input stream
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//        //close input stream
//        inputStream.close();

        //init string to get data from buffer reader
        String line;
        while ((line = bufferedReader.readLine()) != null) {

            result += line + "\n";
        }

//        //close input stream
//        inputStream.close();

        //return result
        return result;

    }

    //do async task
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {

            //init. progress dialog
            progressDialog = ProgressDialog.show(MainActivity.this, "Progressing...", null, true, true);
        }

        @Override
        protected String doInBackground(String... params) {

            //dismiss progress dialog
            progressDialog.dismiss();
            //get url string
            String url = params[0];
            //execute post request adn return result
            return POST(url, person);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast.makeText(MainActivity.this, "Data sent!\n" + s, Toast.LENGTH_LONG).show();
        }
    }
}
